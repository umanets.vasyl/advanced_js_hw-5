import { Card } from "./main_class.js";

const urlApiUsers = "https://ajax.test-danit.com/api/json/users";
const urlApiPosts = "https://ajax.test-danit.com/api/json/posts";

async function getData(urlUser, urlPosts) {
  const response_user = await fetch(urlUser, {
    method: "GET",
  });
  const data_user = await response_user.json();

  const response_posts = await fetch(urlPosts, {
    method: "GET",
  });
  const data_posts = await response_posts.json();
  data_posts.forEach((post) => {
    const { id, userId, title, body } = post;
    let filteredUser = data_user.filter(function (el) {
      return el.id == userId;
    })[0];

    document.body.append(
      new Card(
        id,
        filteredUser.name,
        filteredUser.username,
        filteredUser.email,
        title,
        body
      ).generate()
    );
  });
}

getData(urlApiUsers, urlApiPosts);
